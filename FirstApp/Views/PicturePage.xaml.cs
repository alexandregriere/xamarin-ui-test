﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FirstApp
{
	public partial class PicturePage : ContentPage
	{
		public PicturePage ( )
		{
			InitializeComponent ();
		}

		public void setPictureSource ( UriImageSource source )
		{
			picture.Source = source;
		}

		public void OnBackClicked(object sender, EventArgs args)
		{
			//this.Navigation.PopAsync();
		}
	}
}

