﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FirstApp
{
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();

			SideBar.BackgroundColor = new Color (1,1,1, 0.7);


		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			NavigationPage.SetHasNavigationBar (this, false);
		}

		void test(object sender, EventArgs args)
		{
			var newPage = new DetailView ();
			Navigation.PushAsync (newPage);

		}

		void OnWrapPage10Clicked(object sender, EventArgs e)
		{
			var p = new WrapPage (){
				NumberItems = 10
			};
			Navigation.PushAsync (p);
		}
		void OnWrapPage50Clicked(object sender, EventArgs e)
		{
			var p = new WrapPage (){
				NumberItems = 50
			};
			Navigation.PushAsync (p);
		}
		void OnWrapPage100Clicked(object sender, EventArgs e)
		{
			var p = new WrapPage (){
				NumberItems = 100
			};
			Navigation.PushAsync (p);
		}
		void OnWrapPage500Clicked(object sender, EventArgs e)
		{
			var p = new WrapPage (){
				NumberItems = 500
			};
			Navigation.PushAsync (p);
		}
	}
}

