﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FirstApp
{
	public partial class WrapPage : ContentPage
	{
		public int NumberItems {
			get;
			set;
		}
		public WrapLayout layout {
			get;
			set;
		}

		public WrapPage ()
		{
			InitializeComponent ();

		}

		protected override void OnAppearing ()
		{
			this.layout = new WrapLayout {
				Spacing = 10,
				Padding = new Thickness(5,Device.OnPlatform(20,0,0),5,0),
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.Start,
			};

			this.addItems ();


			ScrollView scrollView = new ScrollView {
				VerticalOptions = LayoutOptions.FillAndExpand, 
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Content = layout
			};


			Content = scrollView;
		}

		private void addItems()
		{
			// simple WrapLayout population
			for (int i = 0; i < this.NumberItems; i++) {
				var s = new StackLayout (){BackgroundColor=new Color(0.5,0.5,0.5)};

				s.Children.Add(new Image(){
					WidthRequest = 155,
					HeightRequest = 150,
					Source = new UriImageSource (){ 

						Uri = new Uri ("http://lorempixel.com/150/120/technics?" + i.ToString()) 
					}
				});
				s.Children.Add(new Label() {
					BackgroundColor = Color.Blue,
					WidthRequest = 150,
					HeightRequest = 20,
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.Center,
					TextColor = Color.Black,
					Text = "Produit " + i.ToString(),
				});

				this.layout.Children.Add (s);

			}
		}
	}
}

