﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FirstApp
{
	public partial class DetailView : ContentPage
	{
		public DetailView ()
		{
			InitializeComponent ();
			this.Title = "Home";
		}

		void OnButtonClicked(object sender, EventArgs args)
		{
			/*
			//DisplayAlert ("Alerte message", "corps du message", "Ok");

			Button a = new Button (){Text="Hey !"};
			a.Clicked += (object s, EventArgs e) => {
				this.OnActionSheetButtonClicked(s, e);
			};

			DisplayActionSheet ("Action sheet", "Annuler", "Destruction");

			this.Content.BackgroundColor = Color.Blue;
			*/
			var p = new PicturePage(  );
			p.setPictureSource (new UriImageSource (){ Uri = new Uri ("http://muscledrive.net/wp-content/uploads/2014/01/muscle-cars-hd-wallpaper-7.jpg") });
			Navigation.PushAsync( p );
		}

		void OnActionSheetButtonClicked(object sender, EventArgs args)
		{
			var newPage = new ContentPage ();
			Navigation.PushAsync (newPage);

		}
			
	}
}